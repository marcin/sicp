#lang sicp
;;;; calculate roots of a cubic polynomial based on the fixed point of a function

(define (square x) (* x x))
(define (cube x) (* x x x))

;;; finds fixed point of a function based on a guess from the sequence x, f(f(x)), f(f(f...)
(define (fixed-point f first-guess)
  (let ((tolerance 0.0001))
  (define (close-enough? v1 v2)
    (< (abs (- v1 v2)) tolerance))
  (define (try guess)
    (let ((next (f guess)))
      (if (close-enough? guess next)
          next
          (try next))))
  (try first-guess)))

;;; approximate derivative of a function by df/dx = (f(x + h) - f(x)) / h
(define (deriv g)
	(let ((dx 0.00001))
		(lambda (x)
		(/ (- (g (+ x dx)) (g x))
   dx))))

;;; calculate netwon transform
(define (newton-transform g)
  (lambda (x)
    (- x (/ (g x) ((deriv g) x)))))

;;; newton method - root of g is a fixed point of the newton's transform
(define (newtons-method g guess)
  (fixed-point (newton-transform g) guess))

(define (cubic a b c)
  (lambda (x) 
    (+ (* x x x)
       (* a x x)
       (* b x)
       c
       )
    ))

(define (roots-of-cubic a b c guess)
  (newtons-method (cubic a b c) guess)
  )

(roots-of-cubic 5 7 9 1)

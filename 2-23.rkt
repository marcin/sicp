#lang racket
;;;; for-each implementation

(define (for-each-sicp f l)
  (f (car l))
  (cond ((not(null? (cdr l)))
        (for-each-sicp f (cdr l)))))

(for-each-sicp (lambda (x) (newline) (display x))
               (list 1 2 3 4 5))


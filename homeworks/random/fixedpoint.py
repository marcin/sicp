import numpy as np
import matplotlib.pyplot as plt

# Approximate fixed point of a function and plot 

def fixed_point_approx(fun, guess, precision):
    fixedpoint = guess
    funval = fun(guess)
    xlist = []
    ylist = []

    while(abs(fixedpoint - funval) > precision):
        fixedpoint = funval
        funval = fun(fixedpoint)
        xlist.append(fixedpoint)
        ylist.append(funval)

    return fixedpoint, xlist, ylist

point, approx_xs, approx_ys = fixed_point_approx(np.cos, -np.pi, 0.01)
        
print(point)
print(approx_xs)
print(approx_ys)
xs = np.linspace(-np.pi/2, np.pi/2, 100)
plt.plot(xs, np.cos(xs))
plt.step(approx_xs,  approx_ys)
plt.show()
